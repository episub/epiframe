package epiframe

import (
	"log"
	"reflect"
	"runtime"
	"testing"
)

var testRoutes = Routes{"home": Route{
	"home",
	[]string{"POST", "GET"},
	"/",
	AppendHandlers(SessionMiddleware),
	true,
},
}

func TestNotFoundDefaultHandlers(t *testing.T) {
	r, err := NewRouter(testRoutes, "TESTCOOKIE", map[string]interface{}{})

	if err != nil {
		t.Error(err)
		return
	}

	if r == nil {
		t.Error("router was nil")
		return
	}

	notFoundHandler := r.NotFoundHandler

	ch, ok := notFoundHandler.(ContextHandler)
	if !ok {
		t.Errorf("notFoundHandler should be ContextHandler")
		return
	}

	expected := 2
	if len(ch.handlers) != expected {
		t.Errorf("ch should have only %d handlers, but has %d.  See output for list", expected, len(ch.handlers))
		log.Printf("Handlers found:")
		for _, h := range ch.handlers {
			name := runtime.FuncForPC(reflect.ValueOf(h).Pointer()).Name()
			log.Printf("* %s", name)
		}
		return
	}

	expectedHandlers := []string{
		"bitbucket.org/episub/epiframe.allMiddleware",
		"bitbucket.org/episub/epiframe.renderNotFoundHandler",
	}

	for i, h := range ch.handlers {
		name := runtime.FuncForPC(reflect.ValueOf(h).Pointer()).Name()

		if name != expectedHandlers[i] {
			t.Errorf("Expected handler %d to be %s, but was %s", i, expectedHandlers[i], name)
		}
	}
}
