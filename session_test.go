package epiframe

import (
	"errors"
	"fmt"
	"net/http"
	"testing"
	"time"
)

func init() {
	InitSession([]byte("very-secretffavz"), []byte("replace-mefalfpwifuanxiflmk;esab"))
}

const TEST_COOKIE_NAME = "test_cookie"

func TestGetSession(t *testing.T) {
	s, err := GetSession(TEST_COOKIE_NAME, nil)

	if s == nil {
		t.Errorf("Session returned should not be nil")
		return
	}

	if err != nil {
		t.Error(err)
		return
	}

	if s.values == nil {
		t.Errorf("Session values should not be nil")
	}

	// tz := s.Get("tz")
	//
	// if tz != DEFAULT_TIMEZONE {
	// 	t.Errorf("Timezone should be Australia/Melbourne, but is %s", tz)
	// }

	// Check the cookie content has been written:
	// err = testCookieHeaderSetValue("tz", DEFAULT_TIMEZONE, s.Cookie())
	//
	// if err != nil {
	// 	t.Error(err)
	// 	return
	// }
}

var testValues = map[string]string{
	"one": "something",
	"two": "someotherthing",
}

func TestGetSessionWithCookie(t *testing.T) {
	// Tests that session restores correct values

	var cookie *http.Cookie
	// Create the cookie:
	if encoded, err := sc.Encode(TEST_COOKIE_NAME, testValues); err == nil {
		cookie = &http.Cookie{
			Name:  TEST_COOKIE_NAME,
			Value: encoded,
		}
	} else {
		t.Error(err)
		return
	}

	s, err := GetSession(TEST_COOKIE_NAME, cookie)

	if err != nil {
		t.Error(err)
		return
	}

	cookie = s.Cookie()

	for k, v := range testValues {
		err := testCookieHeaderSetValue(k, v, cookie)
		if err != nil {
			t.Error(err)
		}
	}
}

func TestSetValue(t *testing.T) {
	key := "testkey"
	value := "testval"

	s, err := GetSession(TEST_COOKIE_NAME, nil)

	if err != nil {
		t.Error(err)
		return
	}

	s.Set(key, value)

	// Check the cookie content has been written:
	err = testCookieHeaderSetValue(key, value, s.Cookie())

	if err != nil {
		t.Error(err)
		return
	}
}

func TestGetValue(t *testing.T) {
	key := "testkey"
	value := "testval"

	s, err := GetSession(TEST_COOKIE_NAME, nil)

	if err != nil {
		t.Error(err)
		return
	}

	s.Set(key, value)

	retrieved := s.Get(key)

	if retrieved != value {
		t.Errorf("Retrieved should be %s but is %s\n", value, retrieved)
	}
}

func TestSetLogin(t *testing.T) {
	username := "bobtest"
	s, err := GetSession(TEST_COOKIE_NAME, nil)

	if s == nil {
		t.Errorf("Session returned should not be nil")
		return
	}

	s.SetLogin(username)

	// Check logged in returns true:

	if !s.LoggedIn() {
		t.Errorf("Not logged in, but should be")
	}

	u := s.Get("u")

	if u != username {
		t.Error("Username is incorrect")
	}

	e := s.Get("e")

	te, err := time.Parse("2006-01-02 15:04:05.999999999 -0700 MST", e)
	if err != nil {
		t.Error(err)
		return
	}

	if time.Now().After(te.Add(sessionExpire - time.Second*5)) {
		t.Error("Expiry not set right")
	}
}

func TestSetLogout(t *testing.T) {
	username := "bobtest"
	s, err := GetSession(TEST_COOKIE_NAME, nil)

	if err != nil {
		t.Error(err)
		return
	}

	if s == nil {
		t.Errorf("Session returned should not be nil")
		return
	}

	s.SetLogin(username)

	// Check logged in returns true:

	if !s.LoggedIn() {
		t.Errorf("Not logged in, but should be")
	}

	// Now logout and test:
	s.SetLogout()

	u := s.Get("u")

	if u != "" {
		t.Errorf("Username should be nil after logging out")
	}

	if s.LoggedIn() {
		t.Errorf("Should not be logged in after logging out")
	}
}

func TestLoginExpiry(t *testing.T) {
	// TODO: Test that login is removed if cookie expires
	username := "TestLoginExpiry"
	s, err := GetSession(TEST_COOKIE_NAME, nil)

	if s == nil {
		t.Errorf("Session returned should not be nil")
		return
	}

	if err != nil {
		t.Error(err)
	}

	s.SetLogin(username)

	// Check logged in returns true:

	if !s.LoggedIn() {
		t.Errorf("Not logged in, but should be")
	}

	s.Set("e", time.Now().String())

	if s.LoggedIn() {
		t.Errorf("Should not be logged in, but is")
	}
}

func testCookieHeaderSetValue(key string, value string, cookie *http.Cookie) error {

	var values map[string]string

	err := sc.Decode(TEST_COOKIE_NAME, cookie.Value, &values)
	if err != nil {
		return err
	}

	if values[key] != value {
		return errors.New(fmt.Sprintf("Retrieved cookie value does not match provided value: %s vs %s", values[key], value))
	}

	return nil
}
