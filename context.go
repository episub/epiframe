package epiframe

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
)

// RequestStatus JSend reply types
type RequestStatus string

var (
	// RequestSuccess Success
	RequestSuccess RequestStatus = "success"
	// RequestFail Failure
	RequestFail RequestStatus = "fail"
	// RequestError Error
	RequestError RequestStatus = "error"
)

var ErrNoTemplate = fmt.Errorf("No such template found")

type Context struct {
	Session          *Session
	Route            Route
	CookieName       string
	Done             bool
	Template         string // Lists name of template to render
	UseErrorTemplate bool   // Whether or not to use a template for rendering error
	data             map[string]interface{}
	Logger           *logrus.Entry
	FinalHandler     func(*Context, http.ResponseWriter, *http.Request)
}

func (c *Context) SetDatum(key string, value interface{}) {
	if c.data == nil {
		c.data = make(map[string]interface{})
	}

	c.data[key] = value
}

func (c *Context) GetDatum(key string) interface{} {
	if c.data == nil {
		c.data = make(map[string]interface{})
	}

	return c.data[key]
}

// One of the render functions must be called by a handler
func (c *Context) RenderTemplate(w http.ResponseWriter, template string) {
	c.prepareRender(w)

	err := c.RenderTemplateToWriter(w, template)

	if err != nil {
		if err == ErrNoTemplate {
			c.RenderError(w, fmt.Errorf("Could not find template with name %s\n", template), http.StatusInternalServerError)
			return
		}

		c.Logger.Error(err)
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	// if c.data == nil {
	// 	c.data = make(map[string]interface{})
	// }
	//
	// data := Data{nil, c.data}
	// page := templates[template]
	//
	// if page != nil {
	//
	// 	err := page.ExecuteTemplate(w, template, data)
	// 	if err != nil {
	// 		c.Logger.Error(err)
	// 		http.Error(w, "Internal error", http.StatusInternalServerError)
	// 		return
	// 	}
	// } else {
	// 	// No such template, got to render that as an internal error
	// 	c.RenderError(w, fmt.Errorf("Could not find template with name %s\n", template), http.StatusInternalServerError)
	// }
}

func (c *Context) RenderTemplateToWriter(w io.Writer, template string) error {
	if c.data == nil {
		c.data = make(map[string]interface{})
	}

	data := Data{nil, c.data}
	page := templates[template]

	if page != nil {

		return page.ExecuteTemplate(w, template, data)
		// if err != nil {
		// 	c.Logger.Error(err)
		// 	http.Error(w, "Internal error", http.StatusInternalServerError)
		// 	return
		// }
	}

	return ErrNoTemplate
}

func (c *Context) RenderRedirect(w http.ResponseWriter, r *http.Request, path string, code int) {
	c.Logger.WithFields(logrus.Fields{
		"path": path,
		"code": code,
	}).Info("Redirecting")

	c.prepareRender(w)

	http.Redirect(w, r, path, code)
}

func (c *Context) RenderError(w http.ResponseWriter, err error, code int) {

	tlog := c.Logger.WithFields(logrus.Fields{
		"useErrorTemplate": c.UseErrorTemplate,
		"code":             code,
	})

	if code >= 500 {
		tlog.WithField("error", err).Error("Rendering Error")
	} else {
		tlog.WithField("error", err).Warn("Rendering Error")
	}

	c.prepareRender(w)

	c.Done = true

	c.SetDatum("errorCode", code)

	if c.data == nil {
		c.data = make(map[string]interface{})
	}

	data := Data{nil, c.data}
	page := templates["error.html"]

	if page != nil && c.UseErrorTemplate {
		w.WriteHeader(code)

		err := page.ExecuteTemplate(w, "error.html", data)
		if err != nil {
			tlog.WithField("error", err).Error("Failed to render error")
			http.Error(w, "", code)
		}

		return
	}

	// No such template, got to render that as an internal error
	if c.UseErrorTemplate {
		tlog.Error("No template found")
	}
	http.Error(w, fmt.Sprintf("%d", code), code)
	return
}

// RenderJSendReply Sends a json reply according to https://labs.omniti.com/labs/jsend.  err is locally logged, while message is sent to client
func (c *Context) RenderJSendReply(w http.ResponseWriter, status RequestStatus, data interface{}, message string, code int, err error) error {
	if status == RequestError {
		// Serious error, so log it:
		c.Logger.WithFields(logrus.Fields{"error": err, "message": message}).Error("RenderJSend error")
	}

	if data == nil && (status == RequestSuccess || status == RequestFail) {
		return fmt.Errorf("Data is required for a message status of type %s", status)
	}

	var reply = make(map[string]interface{})

	reply["status"] = status
	if data != nil {
		reply["data"] = data
	}

	if len(message) > 0 {
		reply["message"] = message
	}

	if code > 0 {
		reply["code"] = code
	}

	body, err := json.Marshal(&reply)

	if err != nil {
		return err
	}

	w.Header().Add("Content-Type", "application/json")
	fmt.Fprintf(w, "%s", body)

	return nil
}

func renderNotFoundHandler(c *Context, w http.ResponseWriter, r *http.Request) {
	c.RenderError(w, fmt.Errorf("renderNotFoundHandler: %s not found", r.RequestURI), http.StatusNotFound)
}

func (c *Context) SetCookie(w http.ResponseWriter) {
	if c.Session != nil {
		cookie := c.Session.Cookie()

		if cookie != nil {
			// c.Logger.Error("setting cookie")
			http.SetCookie(w, cookie)
		}
	} else {
		c.Logger.Warn("No session available")
	}
}

func (c *Context) prepareRender(w http.ResponseWriter) {
	if len(cookie_name) > 0 {
		c.SetCookie(w)
	}
	c.Done = true
}

type ContextHandler struct {
	handlers []func(*Context, http.ResponseWriter, *http.Request)
	route    Route
}

func (c ContextHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := Context{CookieName: cookie_name, Route: c.route}

	ctx.UseErrorTemplate = c.route.UseErrorTemplate

	for _, h := range c.handlers {
		if !ctx.Done {
			h(&ctx, w, r)
		}
	}

	if ctx.FinalHandler != nil {
		ctx.FinalHandler(&ctx, w, r)
	}
}
