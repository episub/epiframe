package epiframe

import (
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/gorilla/mux"
)

var cookie_name string

var templates map[string]*template.Template

var templates_base *template.Template
var temp_pages map[string]string

var Logger *logrus.Entry = logrus.New().WithField("epiframe", true)

var templateFileTypes = map[string]bool{
	TEMPLATE_PAGE_EXT:    true,
	TEMPLATE_SNIPPET_EXT: true,
}

const (
	TEMPLATES_FOLDER     = "views"
	TEMPLATE_PAGE_EXT    = ".html"
	TEMPLATE_SNIPPET_EXT = ".tmpl"
)

type Route struct {
	Name             string
	Methods          []string
	URL              string
	Handlers         []func(*Context, http.ResponseWriter, *http.Request)
	UseErrorTemplate bool
}

type Routes map[string]Route

type Data struct {
	Messages []string
	Data     map[string]interface{}
}

func NewRouter(routes Routes, cookie string, newTemplateFuncs map[string]interface{}) (*mux.Router, error) {

	var templateFuncs = map[string]interface{}{}

	for k, v := range newTemplateFuncs {
		if templateFuncs[k] == nil {
			templateFuncs[k] = v
		} else {
			return nil, fmt.Errorf("Template named %s already exists.  Please use a different template name", k)
		}
	}
	cookie_name = cookie

	router := mux.NewRouter().StrictSlash(true)

	// Serve files from the static 'public' folder:
	router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("./public/"))))

	for _, route := range routes {
		var handler = ContextHandler{handlers: route.Handlers, route: route}

		if route.URL == "/public" {
			return nil, errors.New("The path /public is reserved")
		}

		router.
			Methods(route.Methods...).
			Path(route.URL).
			Name(route.Name).
			Handler(handler)
	}

	SetNotFoundMiddleware(router, allMiddleware, renderNotFoundHandler)

	// Load templates in view folder.  First, we load all the .tmpl files, and set the .page files aside
	temp_pages = make(map[string]string)

	templateWalk := func(path string, info os.FileInfo, err error) error {
		// Here, we preload all templates in the specified folder
		if info != nil && !info.IsDir() && err == nil && templateFileTypes[filepath.Ext(path)] {
			stripval := TEMPLATES_FOLDER + "/"

			name := strings.Replace(path, stripval, "", 1)

			dat, err := ioutil.ReadFile(path)

			if err != nil {
				Logger.WithFields(logrus.Fields{
					"path": path,
				}).Error("Warning: could not load template\n")
				return nil
			}

			if filepath.Ext(path) == TEMPLATE_SNIPPET_EXT {

				if templates_base == nil {
					// Create the first template
					templates_base, err = template.New(name).Parse(string(dat))
					if err != nil {
						Logger.WithField("name", name).Warning("Failed to parse template")
					}

					// Add the functions:

					templates_base = templates_base.Funcs(templateFuncs)

				} else {
					// Add new templates to this template handler
					templates_base, err = templates_base.New(name).Parse(string(dat))
					if err != nil {
						Logger.WithField("name", name).Warning("Failed to parse template")
					}
				}
			} else if filepath.Ext(path) == TEMPLATE_PAGE_EXT {
				// Set this aside to be handled later
				temp_pages[name] = string(dat)
			} else {
				Logger.Error("Should never reach here")
			}

			// logger.Error("Template %s added as %s\n", path, name)
		}

		if err != nil {
			Logger.WithFields(logrus.Fields{"path": path, "error": err}).Warning("Could not load template")
		}

		return nil
	}

	filepath.Walk(TEMPLATES_FOLDER+"/", templateWalk)

	// Now, we make a copy of the collection of template files, one for each .page, save and apply it
	templates = make(map[string]*template.Template)

	for name, content := range temp_pages {
		tmp, err := templates_base.Clone()

		if err != nil {
			return nil, err
		}

		templates[name], err = tmp.New(name).Parse(content)
		if err != nil {
			return nil, err
		}
	}

	return router, nil
}

// SetNotFoundMiddleware Choose which handlers to run before the Not Found handler
func SetNotFoundMiddleware(router *mux.Router, mw ...func(*Context, http.ResponseWriter, *http.Request)) {
	list := []func(*Context, http.ResponseWriter, *http.Request){}

	for _, h := range mw {
		list = append(list, h)
	}

	logrus.WithField("handlers", fmt.Sprintf("%+v", list)).Infof("Setting handlers list")
	router.NotFoundHandler = ContextHandler{handlers: list}
}
