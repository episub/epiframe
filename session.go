package epiframe

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/gorilla/securecookie"
	"github.com/twinj/uuid"
)

var hashKey = []byte("very-secretffavz")

// Block keys should be 16 bytes (AES-128) or 32 bytes (AES-256) long.
// Shorter keys may weaken the encryption used.
var blockKey = []byte("replace-mefalfpwifuanxiflmk;esab")

var sc *securecookie.SecureCookie

func InitSession(hk []byte, bk []byte) {

	hashKey = hk
	blockKey = bk

	sc = securecookie.New(hashKey, blockKey)
}

type Session struct {
	values map[string]string // We only want values to be manipulated through functions here, and not directly
	Name   string
}

var (
	ErrSessionNil = errors.New("Session has not been initialised")
)

const (
	sessionExpire = time.Hour * 48
	// DEFAULT_TIMEZONE = "Australia/Melbourne"
	timeStringFormat = "2006-01-02 15:04:05.999999999 -0700 MST"
)

func GetSession(cookie_name string, cookie *http.Cookie) (*Session, error) {
	// Returns current session, or creates and returns a new one, depending on what is available

	// logger.Printf("Getting Session")
	var err error

	var session Session
	var values map[string]string

	if cookie == nil {
		session.Init(cookie_name, nil)
		return &session, nil
	}

	err = sc.Decode(cookie_name, cookie.Value, &values)

	if err != nil {
		// Assume a corrupt cookie, and create a new, making a note
		Logger.Printf("Corrupt cookie: %s\n", err)
		session.Init(cookie_name, nil)
		return &session, nil
	}

	// logger.Printf("Cookie init values: %s", values)

	session.Init(cookie_name, values)

	return &session, err
}

func (s *Session) Init(name string, values map[string]string) {
	// Initialises this session with default values

	if values == nil {
		values = make(map[string]string)
	}

	s.Name = name
	s.values = values

	// tz := s.Get("tz")

	// if len(tz) == 0 {
	// 	s.Set("tz", DEFAULT_TIMEZONE)
	// }

	suuid := s.Get("suuid")

	if len(suuid) == 0 {
		s.Set("suuid", uuid.NewV4().String())
	}
}

func (s *Session) LoggedIn() bool {
	// Returns false if there's an error.  An error of the right sort should be thrown from
	// when the session is retrieved anyway

	username := s.Get("u")
	expiry := s.Get("e")

	if len(username) == 0 {
		return false
	}

	// Check if it's expired, and log out if it has:
	te, err := time.Parse(timeStringFormat, expiry)

	if err != nil || te.Before(time.Now()) {
		if err != nil {
			Logger.WithFields(logrus.Fields{"error": err, "expiry": expiry}).Warning("Could not parse expiry")
		} else {
			Logger.WithField("expiry", expiry).Warning("Login expired, so logging out")
		}

		s.SetLogout()

		return false
	}

	if len(username) > 0 {
		return true
	}

	return false
}

func (s *Session) SetLogin(username string) {
	s.Set("u", username)
	s.Set("e", GetExpireString(sessionExpire))
}

func (s *Session) SetLogout() {
	Logger.WithField("session", s.String()).Info("Logging out")
	s.Set("u", "")
	s.Set("e", "")
}

func (s *Session) Get(key string) string {

	// This check is done simply so we can call Get without needing to return an error
	if s.values == nil {
		s.values = make(map[string]string)
	}

	return s.values[key]

}

func (s *Session) Set(key string, value string) {
	if s.values == nil {
		s.values = make(map[string]string)
	}

	s.values[key] = value

	return
}

func (s *Session) Delete(key string) {
	if s.values == nil {
		s.values = make(map[string]string)
	} else {
		delete(s.values, key)
	}
}

func (s *Session) Cookie() *http.Cookie {
	// Returns the cookie for sending with header
	if s.values == nil {
		s.values = make(map[string]string)
	}

	if s.Name == "" {
		return nil
	}

	// logger.Printf("Writing values %+v\n", s.values)
	if encoded, err := sc.Encode(s.Name, s.values); err == nil {
		cookie := &http.Cookie{
			Name:    s.Name,
			Value:   encoded,
			Path:    "/",
			Expires: time.Now().Add(365 * 24 * time.Hour),
			HttpOnly: true,
			Secure:   true,
		}
		return cookie
	} else {
		Logger.Printf("Error saving: %s\n", err)
		return nil
	}
}

func (s *Session) String() string {
	return fmt.Sprintf("%+v", s.values)
}

func GetExpireString(expireTime time.Duration) string {
	return time.Now().Add(expireTime).Format(timeStringFormat)
}
