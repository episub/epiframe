Epiframe is a website framework for Go, developed by Episub.  It is designed to be lightweight, while giving some useful extra features, including:

* Automatic template loading
* Middleware
* Session management

This extra behind the scenes work allows the code for handlers to be much shorter.

# Templates:

There are two types of allowable template extensions: .tmpl, and .html.  .html files can be thought of as providing a full page, while .tmpl files are template snippets available to all pages.  One .html cannot access the template of another .html, so anything to be shared between pages should be in a .tmpl file.

Templates are automatically loaded when the program is launched, from the view folder.  Every .tmpl and .html file within those folders will be loaded.