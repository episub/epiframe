package epiframe

import (
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"
	"github.com/twinj/uuid"
)

func AppendHandlers(handlers ...func(*Context, http.ResponseWriter, *http.Request)) []func(*Context, http.ResponseWriter, *http.Request) {
	all := []func(*Context, http.ResponseWriter, *http.Request){allMiddleware}

	return append(all, handlers...)
}

func allMiddleware(ctx *Context, w http.ResponseWriter, r *http.Request) {

	ctx.Logger = Logger.WithFields(logrus.Fields{
		"requestID":    uuid.NewV4().String(),
		"URI":          r.RequestURI,
		"forwardedFor": r.Header.Get("X-Forwarded-For"),
		"remoteAddr":   r.RemoteAddr,
		"host":         r.Host,
		"userAgent":    r.UserAgent(),
	})

	if len(r.Referer()) > 0 {
		ctx.Logger = ctx.Logger.WithField("referer", r.Referer())
	}

	ctx.Logger.WithFields(logrus.Fields{
		"method":    r.Method,
		"userAgent": r.UserAgent(),
	}).Info("Request received")

}

func SessionMiddleware(ctx *Context, w http.ResponseWriter, r *http.Request) {

	cookie, _ := r.Cookie(ctx.CookieName)

	session, err := GetSession(ctx.CookieName, cookie)

	if err != nil {
		ctx.Logger.Printf("SessionMiddleware error: %s\n", err)

		ctx.RenderError(w, fmt.Errorf("Internal error"), http.StatusInternalServerError)
		ctx.Done = true
	} else {
		ctx.Session = session
		u := ctx.Session.Get("u")

		if len(u) > 0 {
			ctx.Logger = ctx.Logger.WithField("username", u)
		}

		ctx.Logger.WithField("session", session.String()).Info("Session data ready")
	}

	suuid := ctx.Session.Get("suuid")

	if len(suuid) > 0 {
		ctx.Logger = ctx.Logger.WithField("suuid", suuid)
	}
}
