package epiframe

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"net/url"

	"github.com/gorilla/mux"
)

// GetRecordedRequest creates router, visits URL, and returns the recorded response
func GetRecordedRequest(data url.Values, method string, cookie *http.Cookie, path string, getRouter func() (*mux.Router, error)) (*httptest.ResponseRecorder, *http.Request, error) {
	r, err := http.NewRequest(method, path, bytes.NewBufferString(data.Encode()))
	if err != nil {
		return nil, r, err
	}

	return recordedRequester(r, method, cookie, path, getRouter)
}

// GetRecordedRequestWithBody creates router, visits URL, and returns the recorded response
func GetRecordedRequestWithBody(body []byte, method string, cookie *http.Cookie, path string, getRouter func() (*mux.Router, error)) (*httptest.ResponseRecorder, *http.Request, error) {
	r, err := http.NewRequest(method, path, bytes.NewBuffer(body))
	if err != nil {
		return nil, r, err
	}

	return recordedRequester(r, method, cookie, path, getRouter)
}

func recordedRequester(r *http.Request, method string, cookie *http.Cookie, path string, getRouter func() (*mux.Router, error)) (*httptest.ResponseRecorder, *http.Request, error) {
	if method == http.MethodPost {
		r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	}

	if cookie != nil {
		r.AddCookie(cookie)
	}

	w := httptest.NewRecorder()

	router, err := getRouter()

	if err != nil {
		return w, r, err
	}

	router.ServeHTTP(w, r)

	return w, r, nil
}
